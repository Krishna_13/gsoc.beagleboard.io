---
layout: page
title: About
permalink: /about/
---

![Build Status](https://git.beagleboard.org/gsoc/gsoc.beagleboard.io/badges/main/pipeline.svg)

[gsoc.beagleboard.io](https://gsoc.beagleboard.io)

Status updates for BeagleBoard.org's Google Summer of Code program.

Primary resources:

* [Forum](https://forum.beagleboard.org/c/gsoc)
* [Live chat](https://bbb.io/gsocchat)

2022 contributor blog sites:

* [Greybus for Zephyr](https://gsoc.beagleboard.io/greybus-for-zephyr)
