---
layout: post
title: 'Hello GSoC'
---

A GSoC specific blog where we can put more small updates at [gsoc.beagleboard.io](https://gsoc.beagleboard.io).

This should be a fair example to fork for individual projects to create their own blog posts.

